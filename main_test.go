package inetprobe

import (
	"os"
	"testing"
	"time"
)

func TestPing(t *testing.T) {
	p := NewInetProbe([]string{"icmp:8.8.8.8"}, time.Second)
	defer p.Stop()
	p.Start()

	select {
	case <-p.Alive():
	case err := <-p.Failed():
		t.Log(err)
		t.Fail()
	case <-time.After(time.Second * 2):
		t.Fail()
	}
}

func TestTCP(t *testing.T) {
	p := NewInetProbe([]string{"tcp:manager.advertisim.com:80"}, time.Second)
	defer p.Stop()
	p.Start()

	select {
	case <-p.Alive():
	case err := <-p.Failed():
		t.Log(err)
		t.Fail()
	case <-time.After(time.Second * 3):
		t.Fail()
	}
}

func TestDNS(t *testing.T) {
	p := NewInetProbe([]string{"dns:manager.advertisim.com"}, time.Second)
	defer p.Stop()
	p.Start()

	select {
	case <-p.Alive():
	case err := <-p.Failed():
		t.Log(err)
		t.Fail()
	case <-time.After(time.Second * 3):
		t.Fail()
	}
}

func TestMultipleFail(t *testing.T) {
	p := NewInetProbe([]string{"tcp:172.16.99.34:27654", "dns:dns.name.that.should.fail"}, time.Second)
	defer p.Stop()
	p.Start()

	timeout := time.After(time.Second * 15)
	for {
		select {
		case <-p.Alive():
			t.Fail()
			return
		case err := <-p.Failed():
			t.Log(err)
		case <-timeout:
			return
		}
	}
}

func TestMultipleFailButOneIsCorrect(t *testing.T) {
	p := NewInetProbe([]string{"tcp:172.16.99.34:27654", "dns:dns.name.that.should.fail", "icmp:8.8.8.8"}, time.Second)
	defer p.Stop()
	p.Start()

	timeout := time.After(time.Second * 20)
	select {
	case <-p.Alive():
	case err := <-p.Failed():
		t.Log(err)
	case <-timeout:
		t.Fail()
		return
	}

}

func TestDisableEnv(t *testing.T) {

	os.Setenv("INETPROBE_DISABLED", "true")

	p := NewInetProbe([]string{"icmp:8.8.8.8"}, time.Second)
	defer p.Stop()
	p.Start()

	select {
	case <-p.Alive():
		t.Fail()
	case err := <-p.Failed():
		t.Log(err)
	case <-time.After(time.Second * 2):
	}

	os.Unsetenv("INETPROBE_DISABLED")
}
