# Package for checking internet access.

Supports addresses like:

    "icmp:8.8.8.8"
    "tcp:127.0.0.1:80"
    "tcp:bitbucket.org:443"
    "dns:advertisim.com"

Will return OK if any of the probes returns true


### Warning
ICMP sockets will not work as unprivileged user if the binary doesn't have CAP_NET_RAW capabilities or you tweak the kernel parameter net.ipv4.ping_group_range 