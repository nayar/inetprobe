package inetprobe

import (
	"errors"
	"fmt"
	"net"
	"net/url"
	"os"
	"time"

	pinger "github.com/digineo/go-ping"
	"golang.org/x/net/context"
)

// Probe object
type Probe struct {
	ctx      context.Context
	cancel   context.CancelFunc
	methods  []*url.URL
	interval time.Duration
	alive    chan struct{}
	failed   chan error
}

// NewInetProbe returns a new internet prober
func NewInetProbe(addresses []string, interval time.Duration) *Probe {
	return NewInetProbeWithContext(context.Background(), addresses, interval)
}

// NewInetProbeWithContext returns a new internet prober
func NewInetProbeWithContext(ctx context.Context, addresses []string, interval time.Duration) *Probe {
	p := &Probe{}
	p.alive = make(chan struct{})
	p.failed = make(chan error, len(addresses))
	p.SetAddresses(addresses)
	p.interval = interval
	p.ctx = ctx

	return p
}

// Start begins testing the internet access
func (p *Probe) Start() {
	p.Stop()
	ctx, cancel := context.WithCancel(p.ctx)
	p.cancel = cancel
	go p.testAll(ctx)
}

// Stop stops the internet access test, but doesn't close the chan
func (p *Probe) Stop() {
	if p.cancel != nil {
		p.cancel()
	}
}

// SetInterval sets the interval between tests
func (p *Probe) SetInterval(interval time.Duration) {
	p.interval = interval
}

// SetAddresses receives a slice of
func (p *Probe) SetAddresses(methods []string) {
	p.methods = []*url.URL{}
	for _, a := range methods {
		if u, err := url.Parse(a); err == nil {
			p.methods = append(p.methods, u)
		}
	}
}

// SetInterval sets the interval between tests
func (p *Probe) Interval() time.Duration {
	return p.interval
}

// Alive receives something everytime a probe is successful
func (p *Probe) Alive() <-chan struct{} {
	return p.alive
}

// Failed receives an error everytime a probe fails
func (p *Probe) Failed() <-chan error {
	return p.failed
}

func (p *Probe) testAll(ctx context.Context) {
	var e error = nil
outer:
	for {
		select {
		case <-ctx.Done():
			return

		case <-time.After(p.interval):
			for _, u := range p.methods {
				inetprobe_disabled := os.Getenv("INETPROBE_DISABLED") == "true"
				if err := Check(u, time.Second*5, ctx); err == nil && !inetprobe_disabled {
					p.alive <- struct{}{}
					e = nil
					continue outer
				} else {
					if inetprobe_disabled {
						e = errors.New(fmt.Sprintf("%s probe disabled by ENV", u))
					} else {
						e = err
					}
				}
			}
			if e != nil {
				select {
				case p.failed <- e:
				default:
				}
			}
		}
	}
}

func Check(u *url.URL, timeout time.Duration, parentCtx context.Context) error {
	done := make(chan error, 1)
	ctx, cancel := context.WithTimeout(parentCtx, timeout)
	defer cancel()

	go func() {
		switch u.Scheme {
		case "icmp":
			if p, err := pinger.New("0.0.0.0", ""); err == nil {
				defer p.Close()
				if a, err := net.DefaultResolver.LookupIPAddr(ctx, u.Opaque); err == nil {
					for _, addr := range a {
						if _, err := p.PingContext(ctx, &addr); err == nil {
							done <- nil
							return
						}
					}
				}
			}
		case "tcp":
			var d net.Dialer
			if n, err := d.DialContext(ctx, "tcp", u.Opaque); err == nil {
				n.Close()
				done <- nil
				return
			}

		case "dns":
			if _, err := net.DefaultResolver.LookupIPAddr(ctx, u.Opaque); err == nil {
				done <- nil
				return
			}
		}

		done <- errors.New(fmt.Sprintf("%s offline", u))
		return
	}()

	select {
	case e := <-done:
		return e
	case <-time.After(timeout):
		return errors.New(fmt.Sprintf("%s timeout", u))
	}
}
